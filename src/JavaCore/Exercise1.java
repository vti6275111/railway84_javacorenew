package JavaCore;

public class Exercise1 {

    public void question1(Account account2) {
//    Kiểm tra account thứ 2
//    Nếu không có phòng ban (tức là department == null) thì sẽ in ra text
//"Nhân viên này chưa có phòng ban"
//    Nếu không thì sẽ in ra text "Phòng ban của nhân viên này là …"
        System.out.println("Câu số 1");
        if (account2.departmentId == null) {
            System.out.println("Nhân viên này chưa có phòng ban");

        } else {
            System.out.println("Phòng ban của nhân viên này là: " + account2.departmentId.name);
        }
    }

    public void question2(Account account2) {
        System.out.println("Câu số 2");
//        Kiểm tra account thứ 2
//        Nếu không có group thì sẽ in ra text "Nhân viên này chưa có group"
//        Nếu có mặt trong 1 hoặc 2 group thì sẽ in ra text "Group của nhân viên
//        này là Java Fresher, C# Fresher"
//        Nếu có mặt trong 3 Group thì sẽ in ra text "Nhân viên này là người
//        quan trọng, tham gia nhiều group"
//        Nếu có mặt trong 4 group trở lên thì sẽ in ra text "Nhân viên này là
//        người hóng chuyện, tham gia tất cả các group"
        if (account2.groups.length == 0) {
            System.out.println("Nhân viên này chưa có group");
        } else if (account2.groups.length == 1 || account2.groups.length == 2) {
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("Nhân viên này là người quan trọng");
        } else {
            System.out.println("Nhân viên này là người hóng chuyện");
        }
    }

    public void question3(Account account2) {
        System.out.println("Câu số 3");
        System.out.println(account2.departmentId == null ? "Nhân viên này chưa có phòng ban" : "Phòng ban của nhân viên này là: " + account2.departmentId.name);
    }

    public void question4(Account account1) {
        System.out.println("Câu số 4");
//        Sử dụng toán tử ternary để làm yêu cầu sau:
//        Kiểm tra Position của account thứ 1
//        Nếu Position = Dev thì in ra text "Đây là Developer"
//        Nếu không phải thì in ra text "Người này không phải là Developer"
        System.out.println(account1.positionId.name == PositonName.Dev ? "Đây là Dev" : "Người này không phải Dev");
    }

    public void question5(Group group1) {
        System.out.println("Câu số 5");
//        Lấy ra số lượng account trong nhóm thứ 1 và in ra theo format sau:
//        Nếu số lượng account = 1 thì in ra "Nhóm có một thành viên"
//        Nếu số lượng account = 2 thì in ra "Nhóm có hai thành viên"
//        Nếu số lượng account = 3 thì in ra "Nhóm có ba thành viên"
//        Còn lại in ra "Nhóm có nhiều thành viên"
        int countNumber = group1.accounts.length;
        if (countNumber == 0) {
            System.out.println("Nhòm chưa có thành viên");
        } else {
            switch (countNumber) {
                case 1: {
                    System.out.println("Nhóm có 1 thành viên");
                    break;
                }
                case 2: {
                    System.out.println("Nhóm có 2 thành viên");
                    break;
                }
                case 3: {
                    System.out.println("Nhóm có 3 thành viên");
                    break;
                }
                default:
                    System.out.println("Nhóm có nhiều thành viên");
                    break;
            }
        }
    }

    public void question6(Account account2) {
        System.out.println("Câu số 6");
        //        Kiểm tra account thứ 2
//        Nếu không có group thì sẽ in ra text "Nhân viên này chưa có group"
//        Nếu có mặt trong 1 hoặc 2 group thì sẽ in ra text "Group của nhân viên
//        này là Java Fresher, C# Fresher"
//        Nếu có mặt trong 3 Group thì sẽ in ra text "Nhân viên này là người
//        quan trọng, tham gia nhiều group"
//        Nếu có mặt trong 4 group trở lên thì sẽ in ra text "Nhân viên này là
//        người hóng chuyện, tham gia tất cả các group"
        int number = account2.groups.length;
        switch (number) {
            case 0:
                System.out.println("Nhân viên này chưa có group");
                break;
            case 1:
                System.out.println("Group của nhân viên này là: " + account2.groups[0].name);
                break;
            case 2:
                System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("Nhân viên này là người quan trọng");
                break;
            default:
                System.out.println("Nhân viên này là người hóng chuyện");
                break;
        }
    }

    public void question7(Account account1) {
        System.out.println("Câu số 7");
        //        Kiểm tra Position của account thứ 1
//        Nếu Position = Dev thì in ra text "Đây là Developer"
//        Nếu không phải thì in ra text "Người này không phải là Developer"
        String checkPositon = account1.departmentId.name;
        switch (checkPositon) {
            case "Dev":
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Người này không phải Dev");
                break;

        }
    }

    public void question8(Account[] accounts1) {
        System.out.println("Câu số 8:");
        // In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của họ
        for (Account account : accounts1) {
            System.out.println("Email là: " + account.email);
            System.out.println("Fullname là: " + account.fullName);
            System.out.println("Department là: " + account.departmentId.name);
        }
    }

    public void question9(Department[] departments) {
        System.out.println("Câu số 9:");
        // in ra thông tin các phòng ban bao gồm: id và name
        for (Department department :
                departments) {
            System.out.println("Id là: " + department.id);
            System.out.println("Name là: " + department.name);
        }
    }

    public void question10(Account[] accounts3) {
        System.out.println("Câu số 10:");
        //In ra thông tin các account bao gồm: Email, FullName và tên phòng ban của họ theo định dạng như sau:
        for (int i = 0; i < accounts3.length; i++) {
            System.out.println("Thông tin account thứ " + (i + 1) + " là: ");
            System.out.println("Email: " + accounts3[i].email);
            System.out.println("Fullname: " + accounts3[i].fullName);
            System.out.println("Department: " + accounts3[i].departmentId.name);
        }
    }

    public void question11(Department[] departments) {
        System.out.println("Câu số 11:");
        for (int i = 0; i < departments.length; i++) {
            System.out.println("Thông tin department thứ " + (i + 1) + " là:");
            System.out.println("Id: " + departments[i].id);
            System.out.println("Name: " + departments[i].name);
        }
    }

    public void question12(Department[] departments) {
        System.out.println("Câu số 12:");
        for (int i = 0; i < departments.length; i++) {
            if (i == 2) {
                break;
            }
            System.out.println("Thông tin department thứ " + (i + 1) + " là:");
            System.out.println("Id: " + departments[i].id);
            System.out.println("Name: " + departments[i].name);

        }
    }

    public void question13(Account[] accounts3) {
        System.out.println("Câu số 13:");
        for (int i = 0; i < accounts3.length; i++) {
            if (i == 1) {
                continue;
            }
            System.out.println("Thông tin account thứ " + (i + 1) + " là: ");
            System.out.println("Email: " + accounts3[i].email);
            System.out.println("Fullname: " + accounts3[i].fullName);
            System.out.println("Department: " + accounts3[i].departmentId.name);
        }
    }

    public void question14(Account[] accounts3) {
        System.out.println("Câu số 14:");
        for (int i = 0; i < accounts3.length; i++) {
            if (accounts3[i].id < 4) {
                System.out.println("Thông tin account thứ " + (i + 1) + " là: ");
                System.out.println("Email: " + accounts3[i].email);
                System.out.println("Fullname: " + accounts3[i].fullName);
                System.out.println("Department: " + accounts3[i].departmentId.name);
            }
        }
    }

    public void question15(int a) {
        System.out.println("Câu số 15: ");
        for (int i = 0; i <= a; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    public void question16(int a) {
        System.out.println("Câu số 16: ");
        int i = 0;
        while (i <= a) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
            i++;
        }
    }

    public void question17(Account[] accounts3) {
        System.out.println("Câu số 17:");
        int i = 0;
        while (i < accounts3.length) {
            if (i < 3) {
                System.out.println("Thông tin account thứ " + (i + 1) + " là: ");
                System.out.println("Email: " + accounts3[i].email);
                System.out.println("Fullname: " + accounts3[i].fullName);
                System.out.println("Department: " + accounts3[i].departmentId.name);
            }
            i++;
        }
        //
    }
}

