package JavaCore;

public class Student {
    public int msv;
    public String fullName;
    float[] diem;

    // Tạo method để tính điểm trung bình:
    public float tinhDiem(){
        int size = diem.length;
        float diem1 = diem[0];
        float diem2 = diem[1];
        float diem3 = diem[2];
        float diemTB = (diem1 + diem2 + diem3)/3;
        return diemTB;
    }

    // tạo method đầu vào là 2 số và in ra 2 số đó:
    public void testInput(int number1, int number2){
        System.out.println(number1+number2);

    }

    public int demoMethod(){

        return 5;
    }
}
