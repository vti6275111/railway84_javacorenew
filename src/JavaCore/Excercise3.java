package JavaCore;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Excercise3 {
    public void Question1(Exam exam){
        System.out.println("==================Câu số 1=============");
        Date currentDate =  new Date();
        SimpleDateFormat spdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fomatDate = spdf.format(currentDate);

        System.out.println("Id là: "+ exam.id);
        System.out.println("Code là: "+ exam.code);
        System.out.println("Title là: "+ exam.title);
        System.out.println("Category là: "+ exam.category.name);
        System.out.println("CreateDate là: "+ fomatDate);
    }

    public void Question2(Exam exam){
        System.out.println("==================Câu số 2=============");
        Date currentDate =  new Date();
        SimpleDateFormat spdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String fomatDate = spdf.format(currentDate);

        System.out.println("Id là: "+ exam.id);
        System.out.println("Code là: "+ exam.code);
        System.out.println("Title là: "+ exam.title);
        System.out.println("Category là: "+ exam.category.name);
        System.out.println("CreateDate là: "+ fomatDate);
    }
}
