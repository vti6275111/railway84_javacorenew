package JavaCore;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class Excercise5 {
    Scanner scanner = new Scanner(System.in);

    public void Question1() {
        System.out.println("============Câu số 1===============");
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào số nguyên thứ 1:");
        int a = scanner1.nextInt();
        System.out.println("Số nguyên thứ 1 là: " + a);
        System.out.println("Mời bạn nhập vào số nguyên thứ 2:");
        int b = scanner1.nextInt();
        System.out.println("Số nguyên thứ 2 là: " + b);
        System.out.println("Mời bạn nhập vào số nguyên thứ 3:");
        int c = scanner1.nextInt();
        System.out.println("Số nguyên thứ 3 là: " + c);
    }

    public void Question2() {
        System.out.println("============Câu số 2===============");
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào số thực thứ 1:");
        float a = scanner1.nextFloat();
        System.out.println("Số thực thứ 1 là: " + a);
        System.out.println("Mời bạn nhập vào số thực thứ 2:");
        float b = scanner1.nextFloat();
        System.out.println("Số thực thứ 2 là: " + b);
    }

    public void Question3() {
        System.out.println("============Câu số 3===============");
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào họ và tên:");
        String name = scanner1.nextLine();
        System.out.println("Tên của bạn là: " + name);
    }

    public void Question5() {
        System.out.println("Chương trình tạo Account");
        Account accountNew = new Account();


        System.out.println("Mời bạn nhập vào Id: ");
        int id = scanner.nextInt();

        System.out.println("Mời bạn nhập vào Department: ");
        String Dep = scanner.next();

        System.out.println("Mời bạn nhập vào Position: ");
        int number = scanner.nextInt();
        PositonName pos = null;

        switch (number) {
            case 1:
                pos = PositonName.Dev;
                break;
            case 2:
                pos = PositonName.Test;
                break;
            case 3:
                pos = PositonName.Scrum_Master;
                break;
            case 4:
                pos = PositonName.PM;
                break;
            default:
                System.out.println("Mời bạn nhập lại!");
        }

        System.out.println("Thông tin Account vừa tạo là:");
        System.out.println("Id là: " + id);
        System.out.println("Department là: " + Dep);
        System.out.println("Id là: " + pos);
    }

    public void Question6() {
        System.out.println("Chương trình tạo Department");
        Department departmentNew = new Department();

        System.out.println("Mời bạn nhập vào Id: ");
        int idNew = scanner.nextInt();

        System.out.println("Mời bạn nhập vào tên phòng ban: ");
        String depNew = scanner.next();

        System.out.println("Thông tin Department vừa tạo là:");
        System.out.println("Id là: " + idNew);
        System.out.println("Tên phòng ban là: " + depNew);
    }

    public void Question8() {
        while (true) {
            System.out.println("==========================================");
            System.out.println("Mời bạn nhập vào chức năng muốn sử dụng:");
            System.out.println("1. Tạo Account");
            System.out.println("2. Tạo Department");

            int a = scanner.nextInt();
            if (a == 1 || a == 2) {
                switch (a) {
                    case 1:
                        Question5();
                        break;
                    case 2:
                        Question6();
                        break;
                }
                return;
            } else {
                System.out.println("Mời bạn nhập lại");
            }
        }
    }


    public void Question9(Account[] accounts, Group[] groups) {

        for (Account account : accounts) {
            System.out.println("Các usernames là: " + account.userName);
        }
        System.out.println("Xin mời nhập vào username:");
        String userName = scanner.next();

        for (Group group : groups) {
            System.out.println("Các Group là: " + group.name);
        }
        System.out.println("Xin mời nhập vào Group:");
        String groupName = scanner.next();

        int indexGroup = -1;
        for (int i = 0; i < groups.length; i++) {
            if (groups[i].name.equals(groupName)) {
                indexGroup = i;
            }
        }

        int indexAccount = -1;
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].userName.equals(userName)) {
                indexAccount = i;
            }
        }

        if (indexAccount < 0 || indexGroup < 0) {
            System.out.println("Hãy kiểm tra lại thông tin bạn vừa nhập, không có thông tin Acc hoặc Group này");
        } else {
            for (int j = 0; j < accounts.length; j++) {
                if (accounts[j].userName.equals(userName)) {
                    Group[] grAdd = {groups[indexGroup]};
                    accounts[j].groups = grAdd;
                    System.out.println("Bạn vừa add group:" + accounts[indexAccount].groups[0].name + " cho Acc: " +
                            accounts[indexAccount].userName);
                }
            }
        }
    }

    public void Question10() {
        // Thêm dữ liệu phòng ban:
        Department department1 = new Department();
        department1.id = 1;
        department1.name = "Sale";

        Department department2 = new Department();
        department2.id = 2;
        department2.name = "Marketing";

        Department department3 = new Department();
        department3.id = 3;
        department3.name = "Engineer";

// Thêm dữ liệu position:
        Position position1 = new Position();
        position1.id = 1;
        position1.name = PositonName.Dev;

        Position position2 = new Position();
        position2.id = 2;
        position2.name = PositonName.Test;

        Position position3 = new Position();
        position3.id = 3;
        position3.name = PositonName.Scrum_Master;

        Position position4 = new Position();
        position4.id = 4;
        position4.name = PositonName.PM;


        Account account1 = new Account();
        account1.id = 1;
        account1.email = "letung@gmail.com";
        account1.fullName = "lê tùng";
        account1.userName = "letung1";
        account1.departmentId = department1;
        account1.positionId = position1;
        account1.createDate = LocalDate.now();

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "letung2@gmail.com";
        account2.fullName = "lê tùng2";
        account2.userName = "letung2";
        account2.departmentId = department2;
        account2.positionId = position2;
        account2.createDate = LocalDate.of(2024, 1, 18);

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "letung3@gmail.com";
        account3.fullName = "tùng3";
        account3.userName = "letung3";
        account3.departmentId = department3;
        account3.positionId = position3;
        account3.createDate = LocalDate.of(2023, 12, 12);

        Account account4 = new Account();
        account4.id = 4;
        account4.email = "letung4@gmail.com";
        account4.fullName = "tùng4";
        account4.userName = "letung4";
        account4.departmentId = department3;
        account4.positionId = position4;
        account4.createDate = LocalDate.of(2023, 6, 25);

        // Thêm dữ liệu Group:
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Group1";
        group1.creator = account1;
        group1.createDate = LocalDate.now();
        Account[] accounts1 = {account1, account2};
        group1.accounts = accounts1;

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Group2";
        group2.creator = account2;
        group2.createDate = LocalDate.now();
        Account[] accounts2 = {account1, account3};
        group2.accounts = accounts2;

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Group3";
        group3.creator = account3;
        group3.createDate = LocalDate.now();

        // gán Group cho Account:
        Group[] groups1 = {group1, group2};
        account1.groups = groups1;

        Group[] groups2 = {group1, group2, group3};
        account2.groups = groups2;

        Group[] groups3 = {group2, group3};
        account3.groups = groups3;

        while (true) {
            System.out.println("==========================================");
            System.out.println("Mời bạn nhập vào chức năng muốn sử dụng:");
            System.out.println("1. Tạo Account");
            System.out.println("2. Tạo Department");
            System.out.println("3. Thêm acc vào group");
            int a = scanner.nextInt();
            if (a == 1 || a == 2 || a == 3) {
                switch (a) {
                    case 1:
                        Question5();
                        break;
                    case 2:
                        Question6();
                        break;
                    case 3:
                        Question9(accounts1, groups1);
                }
                System.out.println("Bạn có muốn thực hiện chức năng khác không?");
                String answer = scanner.next();
                if (answer.equals("có")) {
                    continue;
                } else if (answer.equals("không")) {
                    System.out.println("Xin chào!");
                    return;
                }

            } else {
                System.out.println("Mời bạn nhập lại");
            }
        }
    }



    public void Question11() {
        // Thêm dữ liệu phòng ban:
        Department department1 = new Department();
        department1.id = 1;
        department1.name = "Sale";

        Department department2 = new Department();
        department2.id = 2;
        department2.name = "Marketing";

        Department department3 = new Department();
        department3.id = 3;
        department3.name = "Engineer";

// Thêm dữ liệu position:
        Position position1 = new Position();
        position1.id = 1;
        position1.name = PositonName.Dev;

        Position position2 = new Position();
        position2.id = 2;
        position2.name = PositonName.Test;

        Position position3 = new Position();
        position3.id = 3;
        position3.name = PositonName.Scrum_Master;

        Position position4 = new Position();
        position4.id = 4;
        position4.name = PositonName.PM;


        Account account1 = new Account();
        account1.id = 1;
        account1.email = "letung@gmail.com";
        account1.fullName = "lê tùng";
        account1.userName = "letung1";
        account1.departmentId = department1;
        account1.positionId = position1;
        account1.createDate = LocalDate.now();

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "letung2@gmail.com";
        account2.fullName = "lê tùng2";
        account2.userName = "letung2";
        account2.departmentId = department2;
        account2.positionId = position2;
        account2.createDate = LocalDate.of(2024, 1, 18);

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "letung3@gmail.com";
        account3.fullName = "tùng3";
        account3.userName = "letung3";
        account3.departmentId = department3;
        account3.positionId = position3;
        account3.createDate = LocalDate.of(2023, 12, 12);

        Account account4 = new Account();
        account4.id = 4;
        account4.email = "letung4@gmail.com";
        account4.fullName = "tùng4";
        account4.userName = "letung4";
        account4.departmentId = department3;
        account4.positionId = position4;
        account4.createDate = LocalDate.of(2023, 6, 25);

        // Thêm dữ liệu Group:
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Group1";
        group1.creator = account1;
        group1.createDate = LocalDate.now();
        Account[] accounts1 = {account1, account2};
        group1.accounts = accounts1;

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Group2";
        group2.creator = account2;
        group2.createDate = LocalDate.now();
        Account[] accounts2 = {account1, account3};
        group2.accounts = accounts2;

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Group3";
        group3.creator = account3;
        group3.createDate = LocalDate.now();

        // gán Group cho Account:

        Group[] groups2 = {group1, group2, group3};

// ================================================================
        while (true) {
            System.out.println("==========================================");
            System.out.println("Mời bạn nhập vào chức năng muốn sử dụng:");
            System.out.println("1. Tạo Account");
            System.out.println("2. Tạo Department");
            System.out.println("3. Thêm acc vào group");
            System.out.println("3. Thêm acc vào group ngẫu nhiên");
            int a = scanner.nextInt();
            if (a == 1 || a == 2 || a == 3 || a==4) {
                switch (a) {
                    case 1:
                        Question5();
                        break;
                    case 2:
                        Question6();
                        break;
                    case 3:
                        Question9(accounts1, groups2);
                    case 4:
                        AddAccountRandomGroup();
                }
                System.out.println("Bạn có muốn thực hiện chức năng khác không?");
                String answer = scanner.next();
                if (answer.equals("có")) {
                    continue;
                } else if (answer.equals("không")) {
                    System.out.println("Xin chào!");
                    return;
                }

            } else {
                System.out.println("Mời bạn nhập lại");
            }
        }
    }
        public void AddAccountRandomGroup(){
            // Thêm dữ liệu phòng ban:
            Department department1 = new Department();
            department1.id = 1;
            department1.name = "Sale";

            Department department2 = new Department();
            department2.id = 2;
            department2.name = "Marketing";

            Department department3 = new Department();
            department3.id = 3;
            department3.name = "Engineer";

// Thêm dữ liệu position:
            Position position1 = new Position();
            position1.id = 1;
            position1.name = PositonName.Dev;

            Position position2 = new Position();
            position2.id = 2;
            position2.name = PositonName.Test;

            Position position3 = new Position();
            position3.id = 3;
            position3.name = PositonName.Scrum_Master;

            Position position4 = new Position();
            position4.id = 4;
            position4.name = PositonName.PM;


            Account account1 = new Account();
            account1.id = 1;
            account1.email = "letung@gmail.com";
            account1.fullName = "lê tùng";
            account1.userName = "letung1";
            account1.departmentId = department1;
            account1.positionId = position1;
            account1.createDate = LocalDate.now();

            Account account2 = new Account();
            account2.id = 2;
            account2.email = "letung2@gmail.com";
            account2.fullName = "lê tùng2";
            account2.userName = "letung2";
            account2.departmentId = department2;
            account2.positionId = position2;
            account2.createDate = LocalDate.of(2024, 1, 18);

            Account account3 = new Account();
            account3.id = 3;
            account3.email = "letung3@gmail.com";
            account3.fullName = "tùng3";
            account3.userName = "letung3";
            account3.departmentId = department3;
            account3.positionId = position3;
            account3.createDate = LocalDate.of(2023, 12, 12);

            Account account4 = new Account();
            account4.id = 4;
            account4.email = "letung4@gmail.com";
            account4.fullName = "tùng4";
            account4.userName = "letung4";
            account4.departmentId = department3;
            account4.positionId = position4;
            account4.createDate = LocalDate.of(2023, 6, 25);

            // Thêm dữ liệu Group:
            Group group1 = new Group();
            group1.id = 1;
            group1.name = "Group1";
            group1.creator = account1;
            group1.createDate = LocalDate.now();
            Account[] accounts1 = {account1, account2};
            group1.accounts = accounts1;

            Group group2 = new Group();
            group2.id = 2;
            group2.name = "Group2";
            group2.creator = account2;
            group2.createDate = LocalDate.now();
            Account[] accounts2 = {account1, account3};
            group2.accounts = accounts2;

            Group group3 = new Group();
            group3.id = 3;
            group3.name = "Group3";
            group3.creator = account3;
            group3.createDate = LocalDate.now();

            Group[] groups2 = {group1, group2, group3};

// ================================================================

            for (Account account : accounts1) {
                System.out.println("Các usernames là: " + account.userName);
            }
            System.out.println("Xin mời nhập vào username:");
            String userName = scanner.next();

            Random random = new Random();
            int indexGroup = random.nextInt(groups2.length);
            for (int i = 0; i < groups2.length; i++) {
                if (groups2[i].equals(indexGroup)) {
                    indexGroup = i;
                }
            }

            int indexAccount = -1;
            for (int i = 0; i < accounts1.length; i++) {
                if (accounts1[i].userName.equals(userName)) {
                    indexAccount = i;
                }
            }

            if (indexAccount < 0 ) {
                System.out.println("Hãy kiểm tra lại thông tin bạn vừa nhập, không có thông tin Acc hoặc Group này");
            } else {
                for (int j = 0; j < accounts1.length; j++) {
                    if (accounts1[j].userName.equals(userName)) {
                        Group[] grAdd = {groups2[indexGroup]};
                        accounts1[j].groups = grAdd;
                        System.out.println("Bạn vừa add group:" + accounts1[indexAccount].groups[0].name + " cho Acc: " +
                                accounts1[indexAccount].userName);
                    }
                }
            }
        }
}

