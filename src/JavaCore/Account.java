package JavaCore;

import java.time.LocalDate;

public class Account {
    public int id;
    public String email;
    public String fullName;
    public String userName;
    public Department departmentId;
    public Position positionId;
    public LocalDate createDate;
    public Group[] groups;
}
