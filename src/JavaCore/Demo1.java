package JavaCore;

import java.util.Scanner;

public class Demo1 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//      sum();
//      minus();
        while (true){
            System.out.println("======================Menu========================");
            System.out.printf("|%-50s|%n","Mời bạn chọn phép tính:");
            System.out.printf("|%-50s|%n","Phép cộng:");
            System.out.printf("|%-50s|%n","Phép trừ:");
            System.out.printf("|%-50s|%n","Phép nhân:");
            System.out.println("==================================================");
            int choose = scanner.nextInt();

            switch (choose) {
                case 1:
                    sum();
                    break;
                case 2:
                    minus();
                    break;
                case 3:

                    break;
                default:
                    System.out.println("Mời bạn chọn lại!");
                    break;
            }
        }


//        System.out.println("Mời bạn nhập vào số thứ 1: ");
//        int number1 = scanner.nextInt();
//        System.out.println("Mời bạn nhập vào số thứ 2: ");
//        int number2 = scanner.nextInt();
//        System.out.println("Tổng 2 số là: " + sum_ab(number1, number2));
//
//        System.out.println("Mời bạn nhập vào số thứ 1: ");
//        int number3 = scanner.nextInt();
//        System.out.println("Mời bạn nhập vào số thứ 2: ");
//        int number4 = scanner.nextInt();
//        System.out.println("Hiệu 2 số là: " + minus_ab(number3, number4));


    }

    // viết hàm sum(int a, int b)
    public static int sum_ab(int a, int b) {
        int result_sum = a + b;
        return result_sum;
    }

    // viết hàm minus(int a, int b)
    public static int minus_ab(int a, int b) {
        int result_minus = a - b;
        return result_minus;
    }

    // viết hàm nhan_ab(int a, int b)
    public static int nhan_ab(int a, int b) {
        int result_nhan = a * b;
        return result_nhan;
    }


    //


    public static void sum() {
        // chương trình tính tổng 2 số:
        System.out.println("=============Phép cộng=========");
        System.out.println("Mời bạn nhập vào số thứ 1:");
        int a = scanner.nextInt();
        System.out.println("Mời bạn nhập vào số thứ 2:");
        int b = scanner.nextInt();
        int sum = a + b;
        System.out.println("Tổng của 2 số là:" + sum);

    }

    public static void minus() {
        // chương trình tính hiệu 2 số:
        System.out.println("=============Phép trừ=========");
        System.out.println("Mời bạn nhập vào số thứ 1:");
        int x = scanner.nextInt();
        System.out.println("Mời bạn nhập vào số thứ 2:");
        int y = scanner.nextInt();
        int minus = x - y;
        System.out.println("Hiệu của 2 số là:" + minus);

    }
}

