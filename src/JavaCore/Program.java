package JavaCore;

import java.time.LocalDate;
import java.util.Random;


public class Program {


    public static void main(String[] args) {
// Thêm dữ liệu phòng ban:
        Department department1 = new Department();
        department1.id = 1;
        department1.name = "Sale";

        Department department2 = new Department();
        department2.id = 2;
        department2.name = "Marketing";

        Department department3 = new Department();
        department3.id = 3;
        department3.name = "Engineer";

// Thêm dữ liệu position:
        Position position1 = new Position();
        position1.id = 1;
        position1.name = PositonName.Dev;

        Position position2 = new Position();
        position2.id = 2;
        position2.name = PositonName.Test;

        Position position3 =new Position();
        position3.id = 3;
        position3.name = PositonName.Scrum_Master;

        Position position4 = new Position();
        position4.id = 4;
        position4.name = PositonName.PM;

        // Thêm dữ liệu account:

        Account account1 = new Account();
        account1.id = 1;
        account1.email = "letung@gmail.com";
        account1.fullName = "lê tùng";
        account1.userName = "letung1";
        account1.departmentId = department1;
        account1.positionId = position1;
        account1.createDate = LocalDate.now();

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "letung2@gmail.com";
        account2.fullName = "lê tùng2";
        account2.userName = "letung2";
        account2.departmentId = department2;
        account2.positionId = position2;
        account2.createDate = LocalDate.of(2024,1,18);

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "letung3@gmail.com";
        account3.fullName = "tùng3";
        account3.userName = "letung3";
        account3.departmentId = department3;
        account3.positionId = position3;
        account3.createDate = LocalDate.of(2023,12,12);

        Account account4 = new Account();
        account4.id = 4;
        account4.email = "letung4@gmail.com";
        account4.fullName = "tùng4";
        account4.userName = "letung4";
        account4.departmentId = department3;
        account4.positionId = position4;
        account4.createDate = LocalDate.of(2023,6,25);

        // Thêm dữ liệu Group:
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Group1";
        group1.creator = account1;
        group1.createDate = LocalDate.now();
        Account[] accounts1 = {account1, account2};
        group1.accounts = accounts1;

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Group2";
        group2.creator = account2;
        group2.createDate = LocalDate.now();
        Account[] accounts2 = {account1, account3};
        group2.accounts = accounts2;

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Group3";
        group3.creator = account3;
        group3.createDate = LocalDate.now();

        // gán Group cho Account:
        Group[] groups1 = {group1, group2};
        account1.groups = groups1;

        Group[] groups2 ={group1, group2, group3};
        account2.groups = groups2;

        Group[] groups3 ={ group2, group3};
        account3.groups = groups3;

        // Thêm dữ liệu TypeQuestion:

        TypeQuestion type1 = new TypeQuestion();
        type1.id = 1;
        type1.name = TypeQuestionName.Essay;

        TypeQuestion type2 = new TypeQuestion();
        type1.id = 2;
        type1.name = TypeQuestionName.Multiple_Choice;

        // Thêm dữ liệu CataloryQuestion:

        CategoryQuestion category1 = new CategoryQuestion();
        category1.id = 1;
        category1.name = CategoryQuestionName.Java;

        CategoryQuestion category2 = new CategoryQuestion();
        category2.id = 2;
        category2.name = CategoryQuestionName.NET;

        // Thêm dữ liệu Question:

        Question question1 = new Question();
        question1.id = 1;
        question1.content = "content1";
        question1.category = category1;
        question1.creator = account1;
        question1.type = type1;
        question1.createDate = LocalDate.now();

        Question question2 = new Question();
        question2.id = 2;
        question2.content = "content2";
        question2.category = category2;
        question2.creator = account2;
        question2.type = type2;
        question2.createDate = LocalDate.now();

        Question question3 = new Question();
        question3.id = 3;
        question3.content = "content3";
        question3.category = category2;
        question3.creator = account3;
        question3.type = type1;
        question3.createDate = LocalDate.now();

        // Thêm dữ liệu Answer:
        Answer answer1 = new Answer();
        answer1.id = 1;
        answer1.content = "content1";
        answer1.questionId = question1;
        answer1.isCorrect = true;

        Answer answer2 = new Answer();
        answer2.id = 2;
        answer2.content = "content2";
        answer2.questionId = question2;
        answer2.isCorrect = false;

        // Thêm dữ liệu Exam:
        Exam exam1 = new Exam();
        exam1.id = 1;
        exam1.code = "Code1";
        exam1.category = category1;
        exam1.duration = 60;
        exam1.title = "Title1";
        exam1.creator = account1;
        exam1.createDate = LocalDate.now();

        Exam exam2 = new Exam();
        exam2.id = 2;
        exam2.code = "Code2";
        exam2.category = category2;
        exam2.duration = 60;
        exam2.title = "Title2";
        exam2.creator = account2;
        exam2.createDate = LocalDate.now();

        Exam exam3 = new Exam();
        exam3.id = 3;
        exam3.code = "Code3";
        exam3.category = category2;
        exam3.duration = 60;
        exam3.title = "Title3";
        exam3.creator = account3;
        exam3.createDate = LocalDate.now();

        Question[] questions1 = {question1, question2, question3};
        exam1.questions = questions1;

        Question[] questions2 = {question1, question2};
        exam2.questions = questions2;

        //================Bài tập 1========================
//
//       Exercise1 exercise1_1 = new Exercise1();
//        exercise1_1.question1(account2);
//
//        exercise1_1.question2(account2);
//
//        exercise1_1.question3(account2);
//
//        exercise1_1.question4(account1);
//
//        exercise1_1.question5(group1);
//
//        exercise1_1.question6(account2);
//
//        exercise1_1.question7(account1);
//
//        exercise1_1.question8(accounts1);
//
//        Department[] departments = {department1, department2, department3};
//
//        exercise1_1.question9(departments);
//
//        Account[] accounts3 = {account1, account2, account3, account4};
//
//        exercise1_1.question10(accounts3);
//
//        exercise1_1.question11(departments);
//
//        exercise1_1.question12(departments);
//
//        exercise1_1.question13(accounts3);
//
//        exercise1_1.question14(accounts3);
//
//        exercise1_1.question15(20);
//
//        exercise1_1.question16(25);
//
//        exercise1_1.question17(accounts3);

        // =============== Excercise 3=======================

//        Excercise3 excercise3 = new Excercise3();
//        excercise3.Question1(exam1);
//        excercise3.Question2(exam2);


        // =============== Excercise 5=======================
        Excercise5 excercise5 = new Excercise5();
        //excercise5.Question1();
        //excercise5.Question2();
        //excercise5.Question3();
//        excercise5.Question5();
       // excercise5.Question6();
        //excercise5.Question8();
        //excercise5.Question9(accounts1, groups1);
        //excercise5.Question10();

        //excercise5.Question11();

        // =============== Excercise 6=======================
        Excercise6 excercise6 = new Excercise6();
        excercise6.question1(10);

    }
}
